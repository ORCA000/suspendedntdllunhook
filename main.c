/*

PROGRAMMED BY ORCA 6:35 PM 7/6/2022

Get New Ntdll From Suspended Process, To Avoid Hooked Syscalls

*/

#include <Windows.h>
#include <stdio.h>
#include <psapi.h>
#include <winternl.h>
#include <tlhelp32.h>

#define SYSCALL_STUB_SIZE 23
#define PROCESS "notepad.exe"

/*
TODO:
    - UNHOOK CreateProcessA
    - UNHOOK ReadProcessMemory
    - UNHOOK VirtualProtect

    You can do that using hellsgate, ... [OR] something direct and then fixing the syscalls to be from the new ntdll
*/


BOOL CreateSuspendedProcess(LPVOID* Ntdll, SIZE_T* NtdllSize){

    STARTUPINFOA si;
    PROCESS_INFORMATION pi;
    MODULEINFO mi;
    HMODULE hNtdll;
    ZeroMemory(&si, sizeof si);
    ZeroMemory(&pi, sizeof pi);
    ZeroMemory(&mi, sizeof mi);

    
    if(!CreateProcessA(
        "C:\\Windows\\System32\\"PROCESS,
        NULL,
        NULL,
        NULL,
        TRUE,
        CREATE_SUSPENDED,
        NULL,
        NULL,
        &si,
        &pi
    )){ 
        printf("[-] [FAILED] CreateProcessA : %d \n", GetLastError());
        return FALSE;
    }
    //printf("[+] Process Created Successfully With Pid : %d \n", pi.dwProcessId);

    
    hNtdll = GetModuleHandleA("ntdll.dll");
    if (hNtdll != 0)
        GetModuleInformation(GetCurrentProcess(), hNtdll, &mi, sizeof mi);


    LPVOID buffer = HeapAlloc(GetProcessHeap(), 0, mi.SizeOfImage);
    if (buffer == 0){
        printf("[-] [FAILED] HeapAlloc : %d \n", GetLastError());
        return FALSE;
    }


    if (buffer != 0 && !ReadProcessMemory(pi.hProcess, (LPCVOID)mi.lpBaseOfDll, buffer, mi.SizeOfImage, NULL)){
        printf("[-] [FAILED] ReadProcessMemory : %d \n", GetLastError());
        return FALSE;
    }
    

    //printf("[+] Ntdll Is Read To : 0x%p \n", (PVOID)buffer);
    *Ntdll = buffer;
    *NtdllSize = mi.SizeOfImage;

    CloseHandle(pi.hThread);
    TerminateProcess(pi.hProcess, 0);

    if (hNtdll != 0)
        FreeLibrary(hNtdll);

    if (*Ntdll == NULL || *NtdllSize == NULL )
        return FALSE;

    return TRUE;
}



struct MyStruct {
    // this is to save time, (looking for the structs)
    char* DllBaseAddr;
    PIMAGE_DOS_HEADER pDosHeader;
    PIMAGE_NT_HEADERS pNtHeaders;
    PIMAGE_OPTIONAL_HEADER pOptionalHeader;
    PIMAGE_DATA_DIRECTORY pDataDirectory;
    PIMAGE_EXPORT_DIRECTORY pExportDirectory;
    void** ppFunctions;
    WORD* pOrdinals;
    ULONG* pNames;
    // this is to replace RWX memory when freeing
    DWORD lpfOldProtect; 
};
struct MyStruct InitStruct = { 0 };


BOOL InitializeSearch(PVOID pNtdll) {
    FARPROC pAddress = NULL;
    char* DllBaseAddr = (char*)pNtdll;
    InitStruct.DllBaseAddr = DllBaseAddr;
    InitStruct.pDosHeader = (PIMAGE_DOS_HEADER)DllBaseAddr;
    InitStruct.pNtHeaders = (PIMAGE_NT_HEADERS)(DllBaseAddr + InitStruct.pDosHeader->e_lfanew);
    InitStruct.pOptionalHeader = &InitStruct.pNtHeaders->OptionalHeader;
    InitStruct.pDataDirectory = (PIMAGE_DATA_DIRECTORY)(&InitStruct.pOptionalHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT]);
    InitStruct.pExportDirectory = (PIMAGE_EXPORT_DIRECTORY)(DllBaseAddr + InitStruct.pDataDirectory->VirtualAddress);
    InitStruct.ppFunctions = (void**)(DllBaseAddr + InitStruct.pExportDirectory->AddressOfFunctions);
    InitStruct.pOrdinals = (WORD*)(DllBaseAddr + InitStruct.pExportDirectory->AddressOfNameOrdinals);
    InitStruct.pNames = (ULONG*)(DllBaseAddr + InitStruct.pExportDirectory->AddressOfNames);

    if (InitStruct.DllBaseAddr == NULL     || InitStruct.pDosHeader == NULL     || InitStruct.pNtHeaders == NULL ||
        InitStruct.pOptionalHeader == NULL || InitStruct.pDataDirectory == NULL || InitStruct.pExportDirectory == NULL ||
        InitStruct.ppFunctions == NULL     || InitStruct.pOrdinals == NULL      || InitStruct.pNames == NULL ){

        return FALSE;
    }

    return TRUE;
}


DWORD64 Hash(PBYTE str) {
    DWORD64 dwHash = 0x4837291692187346;
    INT c;
    while (c = *str++)
        dwHash = ((dwHash >> 0x2) + dwHash) + c;
    return dwHash;
}

PVOID GetSyscall (CHAR* Syscall) {

    PVOID SyscallStub = malloc(SYSCALL_STUB_SIZE);
    DWORD64 HashedSyscall = Hash((PBYTE)Syscall);

    for (DWORD i = 0; i < InitStruct.pExportDirectory->NumberOfNames; i++) {
        char* szName = (char*)InitStruct.DllBaseAddr + (DWORD_PTR)InitStruct.pNames[i];
        if (Hash(szName) == HashedSyscall && SyscallStub != NULL) {
            memcpy(SyscallStub, (PVOID)(InitStruct.DllBaseAddr + ((ULONG*)(InitStruct.DllBaseAddr + InitStruct.pExportDirectory->AddressOfFunctions))[InitStruct.pOrdinals[i]]), SYSCALL_STUB_SIZE);
            VirtualProtect(SyscallStub, SYSCALL_STUB_SIZE, PAGE_EXECUTE_READWRITE, &InitStruct.lpfOldProtect);
            return SyscallStub;
        }
    }
    return NULL;
}

VOID FreeSyscall(PVOID SyscallStub) {
    DWORD lpfOldProtect;
    VirtualProtect(SyscallStub, SYSCALL_STUB_SIZE, InitStruct.lpfOldProtect, &lpfOldProtect);
    free(SyscallStub);
    //VERY BAD: 
    ZeroMemory(SyscallStub, SYSCALL_STUB_SIZE);
}



// testing with NtAllocateVirtualMemory : 
typedef NTSYSAPI (NTAPI* fnNtAllocateVirtualMemory)(
    HANDLE              ProcessHandle,
    PVOID*              BaseAddress,
    ULONG               ZeroBits,
    PULONG              RegionSize,
    ULONG               AllocationType,
    ULONG               Protect
);


int main() {
    LPVOID Ntdll = NULL;
    SIZE_T NtdllSize = NULL;

    if (!CreateSuspendedProcess(&Ntdll, &NtdllSize)){
        return -1;
    }

    if (!InitializeSearch(Ntdll)){
        return -1;
    }

    // how to call ? :


    // u can run Hash() on 'NtAllocateVirtualMemory' string and pass it by hash to GetSyscall() directly but its a string for simplicity 
    fnNtAllocateVirtualMemory pNtAllocateVirtualMemory = (fnNtAllocateVirtualMemory) GetSyscall("NtAllocateVirtualMemory");
    printf("[+] pNtAllocateVirtualMemory : 0x%p \n", (PVOID) pNtAllocateVirtualMemory);

    // calling NtAllocateVirtualMemory
    PVOID BaseAddress = NULL;
    SIZE_T RegionSize = 0x100;
    pNtAllocateVirtualMemory(GetCurrentProcess(), &BaseAddress, 0, &RegionSize, MEM_COMMIT | MEM_RESERVE, PAGE_READONLY);
    printf("[i] BaseAddress : 0x%p \n", (PVOID) BaseAddress);
    
    FreeSyscall(pNtAllocateVirtualMemory);

    printf("[i] Hit Enter To Exit ... \n");
    getchar();

    return 0;
}

